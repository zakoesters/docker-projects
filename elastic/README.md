# Elastic

This is a docker-compose that will start a two node ElasticSearch instance with Kibana.

### Prerequisites

Ensure that you have both docker and docker-compose installed.


### Starting it up

First, run the the following to se the `vm.max_map_count` or else ElasticSearch will fail to start.

```
sudo sysctl -w vm.max_map_count=262144
```

Next, launch the compose file while in the `elastic` directory.

```
docker-compose up -d
```

After this, ElasticSearch should be available on port 9200 and you should be able to reach Kibana via a browser by going to http://localhost:5601
